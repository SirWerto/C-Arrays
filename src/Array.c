
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "Array.h"

//STRUCT
typedef struct s_array{
	size_t n;	//Number of actual elements
	size_t buff;	//Diff betwin de array and n
	size_t nr;	//Real dimension of the array
	size_t dstr;	//Dimension of the struct
	void * ar;	//The array
}s_array;



//FUNCTIONS

	//CREATE AND DESTROY
array * a_new(size_t d_str){
	array * a =(array*) malloc(sizeof(array));
	if(!a)
		return NULL;
	a->ar = malloc(d_str*BUFF);
	if(!a->ar){
		free(a);
		return NULL;}

	a->n = 0;
	a->buff = BUFF;
	a->nr = BUFF;
	a->dstr = d_str;
return a;
}

void destroy(array * a){
	if(!a)
		return;
	if(!a->ar)
		free(a);
		a = NULL;
		return;
	free(a->ar);	
	a->ar = NULL;	//Proof
	free(a);
	a = NULL;
}

array * clone(array * a){
	if(!a)
		return NULL;
	array * tmp = new(a->dstr);
	if(!tmp || !a->ar)
		return tmp;
	
	tmp->n = a->n;	
	tmp->buff = a->buff;
	tmp->nr = a->nr;
	tmp->dstr = a->dstr;

	tmp->ar = realloc(tmp->ar, tmp->dstr*(tmp->nr+BUFF));
	if(!tmp->ar){
		destroy(tmp);
		return NULL;}
		
	unsigned char * p1 =(unsigned char*) a->ar;
	unsigned char * p2 =(unsigned char*) tmp->ar;
	for(int i=0; i<tmp->n; i++){
		if(!(memcpy(p2, p1, tmp->dstr))){
			destroy(tmp);
			return NULL;}
		p1 += tmp->dstr;
		p2 += tmp->dstr;}

return tmp;
}


    //GETTERS
void * get(array * a, size_t i){
	if(!a || !a->ar || i >= a->n)
		return NULL;
	unsigned char * point =(unsigned char*) a->ar;
	point += i * a->dstr;
	
return point;
}

size_t size(array * a){
	return (a)?a->n:0;
}

bool isempty(array * a){
	return (a && a->ar)? !(a->n): true;
}

size_t dstr(array * a){
	return (a)? a->dstr: 0;
}


	//MODIFIE
//Proteger para no perder la operación si falla
bool add(array * a, void * str){	//Add an element at the end
	if(!a || !a->ar || !str || a->dstr != sizeof(&str))
		return false;
	if(a->buff == 0){
		a->ar = realloc(a->ar, a->dstr*(a->nr+BUFF));
		if(!a->ar)
			return false;
		a->nr += BUFF;
		a->buff += BUFF;
	}
	unsigned char * point =(unsigned char*) a->ar;
	point += a->n * a->dstr;
	if(!(memcpy(point, str, a->dstr)))
		return false;
	a->n++;
	a->buff--;

return true;}	

bool add_i(array * a, void * str, size_t index){
	if(!a->ar || !str || a->n < index || a->dstr != sizeof(&str))
		return false;	
	if(a->buff == 0){
		a->ar = realloc(a->ar, a->dstr*(a->nr+BUFF));
		if(!a->ar)
			return false;
		a->nr += BUFF;
		a->buff += BUFF;
	}
	unsigned char * point =(unsigned char*) a->ar;
	point += a->n * a->dstr; 
	for(int i = a->n; i > index; i--){
		if(!(memcpy(point, point - a->dstr, a->dstr)))
			return false;
		point -= a->dstr;
	}
	if(!(memcpy(point, str, a->dstr)))
		return false;
	a->n++;
	a->buff--;
return true;}

bool remove_o(array * a, void * str){
	if(!a || !a->ar || !str || a->dstr != sizeof(&str))
		return false;
	unsigned char * point =(unsigned char*) a->ar;
	for (int i = 0; i < a->n; i++){
		if(!(memcmp(str, point, a->dstr))){
			for(int j = i; j<a->n; j++){
				if(!(memcpy(point, point+a->dstr, a->dstr)))
					return false;
				point += a->dstr;
			return true;
			} 
		}
	point += a->dstr;
	}

return false;}	

void * remove_i(array * a, size_t index){
	if(!a->ar || a->n < index)
		return NULL;
	unsigned char * point =(unsigned char*) a->ar;
	point += index * a->dstr;
	void * ret = malloc(a->dstr);
	if(!(memcpy(ret,point, a->dstr)))
		return NULL;
	printf("\n%lf",*(double*)ret);
	for(int i = index; i < a->n; i++){
		if(!(memcpy(point, point + a->dstr, a->dstr)))
			return NULL;
		point += a->dstr;
	}
	a->n--;
	a->buff++;
return ret;}


bool a_do(array * a, bool (*do_e)(void * str)){
  if(!a->ar || a->n==0)
    return false;
  array * backup = clone(a);
  if(!backup)
    return false;
  for(int i = 0; i < a->n; i++){
    if(!((*do_e)(get(a,i)))){
      destroy(a);
      a = backup;
      return false;
    }
  }
  destroy(backup);
  return true;
}

    //EQUALS
bool equal_o(void * str1, void * str2, size_t size){
	if(!str1 || !str2 || size == 0)
		return false;
	if(!(memcmp(str1, str2,size)))
		return true;
return false;
}

bool equal(array * a1, array * a2){
	if(!a1 || !a2 || a1->n != a2->n || a1->dstr != a2->dstr)
		return false;
	unsigned char *p1 = a1->ar, *p2 = a2->ar;
	for(size_t i = 0; i < a1->n; i++){
		if((memcmp(p1, p2, a1->dstr)))
			return false;
		p1 += a1->dstr;
		p2 += a1->dstr;
	}
return true;
}


	//SORT
void sort(array * a, bool (*compare)(void * str1, void * str2)){
	if(!a->ar || a->n==0)
		return;
	for(int i = 2; i < a->n; i++){
		for(int j = 0; j < a->n; j++){
			if(((*compare)(get(a,j),get(a,j+1))))
				continue;
			else{

			}
		}
	}

}


	//SHOW
void show(array * a, void(*show_e)(void * str)){
	if(!a || !a->ar) 
		return;
	printf("\n[");
	for(int i = 0; i < a->n; i++){
		printf(" ");
		(*show_e)(get(a,i));
		printf(" ");
	}
	printf("]\n");
}


	//FILTER
array * filt(array * a, bool(*filt_e)(void * str)){
	if(!a->ar || a->n == 0)
		return NULL;
	array * temp_a = new(a->dstr);
	for(int i = 0; i < a->n; i++){
		if(((*filt_e)(get(a,i))))
			add(temp_a, get(a,i));
	}
	
return temp_a;
}


