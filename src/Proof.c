#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "Array.h"
#include "Proof.h"
  
  typedef struct point{
          double x;
          double y;
  }point;
  
void show_e(double * p){
	if(!p)
		return;
printf("%lf", *p);
}

bool filt_e(double * p){
return (*p < 29);
}
  int main(){
 	double t1 = 28, t2 = 53.6, t3 = 5, t4 = 100; 
	void (* show_E)(void * str) = show_e;
	bool (* filt_E)(void * str) = filt_e;
	array * bueno = new(sizeof(double));
	add(bueno, &t1);
	add(bueno, &t2);
	add(bueno, &t3);
	add(bueno, &t4);
	add(bueno, &t1);
	show(bueno, show_E);
	array * copia = clone(bueno);
printf("\n\n");
	show(copia, show_E);
printf("\n\n");
	remove_i(bueno, 3);
	add(bueno, &t1);
	show(bueno, show_E);
	printf("\n%d\n", equal(bueno, copia));

return 0;
}



