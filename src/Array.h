

#ifndef ARRAY_H_
#define ARRAY_H_


//DEFINES
#define BUFF 10

//STRUCT
typedef struct s_array array;

	//size_t n;	//Number of actual elements
	//size_t buff;	//Diff betwin de array and n
	//size_t nr;	//Real dimension of the array
	//size_t dstr;	//Dimension of the struct
	//void * ar;	//The array


//FUNCTIONS

//Create and destroy
array * a_new(size_t d_str);

void destroy(array * a);

array * clone(array * a);


//Getters
void * get(array * a, size_t i);

size_t size(array * a);

bool isempty(array * a);

size_t dstr(array * a);




//Modifie
bool add(array * a, void * str);			//Add an element at the end

bool add_i(array * a, void * str, size_t index);	//Add an element at the end

bool remove_o(array * a, void * str);			//Remove the first equal element

void * remove_i(array * a, size_t index);		//Remove the i element

bool a_do(array * a, bool (*do_e)(void * str));


//Equals
bool equal_o(void * str1, void * str2, size_t size);

bool equal(array * a1, array * a);

//Advance operations

//Trunc and Union
bool uarrays(array * a1, array * a2);

//Sort
void sort(array * a, bool (*compare)(void * str1, void * str2));

//Show
void show(array * a, void(*show_e)(void * str));

//Filter
array * filt(array * a, bool(*filt_e)(void * str));

#endif // ARRAY_H_
